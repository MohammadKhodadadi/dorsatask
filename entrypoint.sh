#!/bin/bash
set -e

#python manage.py makemigrations core
#python manage.py migrate core
#
#python manage.py makemigrations evaluate
#python manage.py migrate evaluate

python manage.py makemigrations
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
# Start the default command provided
exec "$@"


from rest_framework import serializers
from django.db import transaction
from .models import Numbers, Total


class NumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Numbers
        fields = ('a', 'b')

    def save(self, **kwargs):
        with transaction.atomic():
            _sum = self.validated_data['a'] + self.validated_data['b']
            instance = Numbers.objects.create(sum=_sum, **self.validated_data)

            # Inorder to avoid collisions I had to use `select_for_update`
            # to lock data instance and perform the update action.
            last_total = Total.objects.all().select_for_update().first()

            if last_total is None:
                Total.objects.create(total=_sum, sum=instance)
            else:
                Total.objects.filter(pk=last_total.id).update(total=last_total.total + _sum, sum=instance)

            return instance


class TotalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Total
        fields = ('total',)

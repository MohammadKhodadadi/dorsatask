from rest_framework.throttling import SimpleRateThrottle


class IncorrectRequestThrottle(SimpleRateThrottle):
    scope = 'incorrect_requests'

    def get_cache_key(self, request, view):
        if request.method == 'GET':
            return None

        if request.user and request.user.is_authenticated:
            ident = request.user.pk
        else:
            ident = self.get_ident(request)
        return self.cache_format % {
            'scope': self.scope,
            'ident': ident
        }



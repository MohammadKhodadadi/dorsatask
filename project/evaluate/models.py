from django.conf import settings
from django.db import models


class Numbers(models.Model):
    a = models.IntegerField()
    b = models.IntegerField()
    sum = models.BigIntegerField()


class Total(models.Model):
    
    sum = models.OneToOneField(Numbers, on_delete=models.CASCADE)
    total = models.BigIntegerField()

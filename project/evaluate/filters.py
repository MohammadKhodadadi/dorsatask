from django_filters.rest_framework import FilterSet
from .models import Numbers


class NumbersFilter(FilterSet):
    class Meta:
        model = Numbers
        fields = {
            'a': ['exact'],
            'b': ['exact']
        }

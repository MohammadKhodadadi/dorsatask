from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.throttling import AnonRateThrottle
from rest_framework.views import APIView
from rest_framework import status, mixins
from .pagination import CustomPagination
from .serializers import NumberSerializer, TotalSerializer
from .filters import NumbersFilter
from .models import Numbers, Total
from .custom_throttle import IncorrectRequestThrottle


class SummationView(APIView):
    """
    Action: calculate the sum of a and b.
    parameters: a and b must be integers otherwise you will get an error.
    """
    filter_backends = [DjangoFilterBackend]
    filterset_class = NumbersFilter
    throttle_classes = [AnonRateThrottle, IncorrectRequestThrottle]

    def get(self, request):
        query_params = NumberSerializer(data=request.query_params.dict())
        query_params.is_valid(raise_exception=True)
        result = query_params.save()
        return Response({'result': result.sum}, status=status.HTTP_200_OK)


class HistoryView(ListAPIView):
    """
    Action: retrieve history of inputs (a, b)
    """
    queryset = Numbers.objects.all()
    serializer_class = NumberSerializer
    pagination_class = CustomPagination
    throttle_classes = [IncorrectRequestThrottle]
    permission_classes = [IsAdminUser]


class TotalView(RetrieveAPIView):
    """
    Action: get total number from Total data model.
    """
    queryset = Total.objects.all()
    serializer_class = TotalSerializer
    permission_classes = [IsAdminUser]
    throttle_classes = [IncorrectRequestThrottle]

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.queryset.first())
        return Response(serializer.data)

from django.urls import path, include
from . import views
from rest_framework import routers

# router = routers.DefaultRouter()
# router.register('summation', views.SummationView, basename='summation')

urlpatterns = [

    path('sum/', views.SummationView.as_view(), name='Summation'),
    path('history/', views.HistoryView.as_view(), name='History'),
    path('total/', views.TotalView.as_view(), name='Total'),
]

## Installation
- Make and activate virtual environment and install all dependencies have been written in `requirements.txt`.
- Make sure to choose suitable Database(Mysql Postgres) before running the django server.
- If you desire to change some configuration related to `Throttle`  you can find it in  `DEFAULT_THROTTLE_RATES` that is located 
 under the `REST_FRAMEWORK` constant variable.
- If you like to change access or refresh tokens lifetime then you can find it
`SIMPLE_JWT` (note: Use `JWT` as prefix for inserting new access token in header)
## All functions which have been performed in the system
- User is able to sign up , sign in (Djoser package)
- Authentication system has been implemented (JWT token)
- Swagger has been provided in `/docs` endpoint.
- some endpoints need to be admin user (username: admin, password: admin)
like `/history` and `/total` endpoints. (please make accress token for admin user and set in header 
as `Authorization` key and prefix value `JWT`)
- Inorder to create new access token you can request to `/auth/jwt/create` 
- the signal.py that is located in `evaluate` application and its action is
to create `admin` user with password `admin` (as you explained in the description above)
## Run Locally
Run the following command in virtual environment
1) `python manage.py makemigrations` 
2) `python manage.py migrate`
3) `python manage.py runserver`
- **Note**: this project will run in development mode then you can set Debug mode to `False` and so on. 
## Run by docker-compose
Just run the following command
- `docker-compose up --build`
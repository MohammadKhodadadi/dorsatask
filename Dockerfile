FROM python:3.9

# Set environment variables
ENV PYTHONUNBUFFERED 1

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Copy entrypoint.sh to /usr/local/bin and change permission accordignly
COPY entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.sh



# Install any needed packages specified in requirements.txt

RUN --mount=type=cache,target=/root/.cache/pip pip install -r requirements.txt


WORKDIR /app/project


# Exectue entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]


# Expose port 8000 for the Django application
EXPOSE 8000

